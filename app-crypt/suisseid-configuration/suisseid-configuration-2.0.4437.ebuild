# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit unpacker

MY_PV="2.0.4437-1"
MY_P="${PN}_${MY_PV}"

DESCRIPTION="Configure firefox, evolution and thunderbird for SuisseID."
HOMEPAGE="https://postsuisseid.ch"
SRC_URI_COMMON="http://update.swisssign.com/media/stick/repository/dists/jessie/non-free/binary-"
SRC_URI="amd64? ( ${SRC_URI_COMMON}amd64/${MY_P}_amd64.deb )
	x86? ( ${SRC_URI_COMMON}i386/${MY_P}_i386.deb )
"

LICENSE="suisseid"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="mirror"
QA_PREBUILT="*"

RDEPEND="app-crypt/suisseid-pkcs11
	dev-libs/nss[utils]
"
DEPEND="${RDEPEND}
	virtual/awk
"

S=${WORKDIR}/

src_install() {
	into /usr
	dobin usr/bin/suisseid-user-configuration.sh
	dobin usr/bin/suisseid-undo-user-configuration.sh

	dosbin usr/sbin/suisseid-system-defaults.sh
	dosbin usr/sbin/suisseid-undo-system-defaults.sh

	insinto /usr/share/doc/${PF}
	doins usr/share/doc/${PN}/*
}

pkg_postinst() {
	elog "To automatically configure the system for SuisseID usage, call"
	elog "  emerge --config ${CATEGORY}/${PN}"
	elog ""
	elog "A manual configuration is also possible:"
	elog "To add the SuisseID pkcs11 module to global crypto databases, call"
	elog "  suisseid-system-defaults.sh"
	elog "as root user."
	elog "To add the SuisseID pkcs11 module to a user's crypto databases, call"
	elog "  suisseid-user-configuration.sh"
	elog "as appropriate user"
	elog "To reverse the above commands, use"
	elog "  suisseid-undo-system-defaults.sh and suisseid-undo-user-configuration.sh respectively"
}

pkg_prerm() {
	elog "Cleanup SuisseID from system defaults and from all users (1000 ≤ ID ≤ 65000)"
	/usr/sbin/suisseid-undo-system-defaults.sh || die "error while calling suisseid-undo-system-defaults.sh"

	for user in $(getent passwd | awk -F: '$3>=1000&&$3<65000 {print $1}'); do
		elog "  user: $user"
		su - "$user" -c "/usr/bin/suisseid-undo-user-configuration.sh -b" || die "error while calling suisseid-undo-user-configuration.sh"
	done
}

pkg_config() {
	elog "Setup SuisseID for system defaults and for all users (1000 ≤ ID ≤ 65000)"
	/usr/sbin/suisseid-system-defaults.sh < /dev/null || die "error while calling suisseid-system-defaults.sh"

	for user in $(getent passwd | awk -F: '$3>=1000&&$3<65000 {print $1}'); do
		elog "  user: $user"
		su - "$user" -c "/usr/bin/suisseid-user-configuration.sh -b < /dev/null" || die "error while calling suisseid-user-configuration.sh"
	done
}
