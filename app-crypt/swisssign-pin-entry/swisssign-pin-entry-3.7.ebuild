# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit eutils unpacker

MY_PV="3.7.-200~jessie.275"
MY_P="${PN}_${MY_PV}"

DESCRIPTION="SuisseID Pin Entry"
HOMEPAGE="https://postsuisseid.ch"
SRC_URI_COMMON="http://update.swisssign.com/media/stick/repository/dists/jessie/non-free/binary-"
SRC_URI="amd64? ( ${SRC_URI_COMMON}amd64/${MY_P}_amd64.deb )
	x86? ( ${SRC_URI_COMMON}i386/${MY_P}_i386.deb )
"

LICENSE="suisseid"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="mirror"
QA_PREBUILT="*"

RDEPEND="app-crypt/p11-kit
	dev-libs/libpcscxx
	dev-libs/openssl:0
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtwidgets:5
	sys-apps/pcsc-lite
"
DEPEND="${RDEPEND}"

S=${WORKDIR}/

src_install() {
	into /usr
	for app in swisssign-pin-entry swisssign-pin-entry-lcb-test swisssign-pin-entry-sign-test; do
		dobin usr/bin/${app}
	done

	dolib.so usr/lib/*

	insinto /usr/share/doc/${PF}
	doins -r usr/share/doc/${PN}/*
}
