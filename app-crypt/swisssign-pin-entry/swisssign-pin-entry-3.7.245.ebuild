# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit eutils unpacker

MY_PV="${PV}~bionic.319"
MY_P="${PN}_${MY_PV}"

DESCRIPTION="SuisseID Pin Entry"
HOMEPAGE="https://postsuisseid.ch"
SRC_URI_COMMON="https://update.swisssign.com/repo/pool/main/s/${PN}"
SRC_URI="amd64? ( ${SRC_URI_COMMON}/${MY_P}_amd64.deb )
	x86? ( ${SRC_URI_COMMON}/${MY_P}_i386.deb )
"

LICENSE="suisseid"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="mirror"
QA_PREBUILT="*"

RDEPEND="app-crypt/p11-kit
	dev-libs/libpcscxx
	dev-libs/openssl:0
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtwidgets:5
	sys-apps/pcsc-lite
"
DEPEND="${RDEPEND}"

S=${WORKDIR}/

src_install() {
	dolib.so usr/lib/*

	insinto /usr/share/doc/${PF}
	doins -r usr/share/doc/${PN}/*
}
